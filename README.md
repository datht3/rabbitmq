# RabbitMQ Simple Project
## Requirement
- Install RabbitMQ with docker or install directly into machine
- Node version >= 14.6.x
## Describe
![alt text](https://res.cloudinary.com/xanhz123/image/upload/v1660361443/RabbitMQ.drawio_mx2vjy.png)
## Installation
```sh
git clone git@gitlab.com:datht3/rabbitmq.git
cd rabbitmq
yarn
```
## How to run
First:
```sh
Rename .env.example to .env and run RabbitMQ in your machine
```
Run cook service:
```sh
yarn run cook
```
Run eat service:
```sh
yarn run eat
```
Run wash dish service:
```sh
yarn run wash
```
