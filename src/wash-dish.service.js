const amqp = require('amqplib')
const configs = require('./config')
const getCurrentTime = require('./time')
const { uri, queue_wash, exchange } = configs.rabbit

try {
	run()
} catch (error) {
	console.log(error)
	process.exit(0)
}

async function run() {
	const connection = await amqp.connect(uri)
	const channel = await connection.createChannel()

  channel.assertExchange(exchange, 'direct', {
    durable: true
  })

	channel.assertQueue(queue_wash, {
		durable: true,
	})

  channel.bindQueue(queue_wash, exchange, 'wash')

  channel.prefetch(1)

	channel.consume(queue_wash, function (msg) {
			console.log(`[x] Received message: ${msg.content.toString()} --- Time: ${getCurrentTime()}`)
			setTimeout(() => {
				console.log(`[x] Done at ${getCurrentTime()}`)
        channel.ack(msg)
			}, 10 * 1000)
		}, {
			noAck: false,
		}
	)
}
