const amqp = require('amqplib')
const configs = require('./config')
const schedule = require('node-schedule')
const getCurrentTime = require('./time')
const rule = '5 * * * * *'
const { uri, exchange } = configs.rabbit
const content = Buffer.from('Rice')

try {
	run()
} catch (error) {
	console.log(error)
	process.exit(0)
}

process.on('SIGINT', () => {
	schedule.gracefulShutdown().then(() => process.exit(0))
})


async function run() {
	const connection = await amqp.connect(uri)
	const channel = await connection.createChannel()

  // declare exchange type direct
  channel.assertExchange(exchange, 'direct', {
    durable: true, // exchange won't be lost even if connection is closed
  })

  // publish message following by rule
  const job = schedule.scheduleJob(rule, () => {
    channel.publish(exchange, 'eat', content)
    console.log(`[x] Message was sent at ${getCurrentTime()} ----- Content: ${content.toString()}`)
  })
}


