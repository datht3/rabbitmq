const amqp = require('amqplib')
const configs = require('./config')
const getCurrentTime = require('./time')
const { uri, queue_eat, queue_wash, exchange } = configs.rabbit

try {
	run()
} catch (error) {
	console.log(error)
	process.exit(0)
}

async function run() {
	const connection = await amqp.connect(uri)
	const channel = await connection.createChannel()

	// Declare exchange type direct
  channel.assertExchange(exchange, 'direct', {
    durable: true, // exchange won't be lost even if connection is closed
  })

	// Declare queue
	channel.assertQueue(queue_eat, {
		durable: true, // queue will still exist even if connection is closed
	})

	// Bind queue to exchange
  channel.bindQueue(queue_eat, exchange, 'eat')

  channel.prefetch(1) // Comsume one message at one moment

	channel.consume(queue_eat, function (msg) {
			console.log(`[x] Received message: ${msg.content.toString()} --- Time: ${getCurrentTime()}`)
			setTimeout(() => {
				console.log(`[x] Done at ${getCurrentTime()}`)
        channel.ack(msg)

        console.log(`[x] Sending to wash`)
        channel.publish(exchange, 'wash', Buffer.from(msg.content))
			}, 10 * 1000)
		}, {
			noAck: false, // manual ack
		}
	)
}
