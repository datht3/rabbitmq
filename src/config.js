const dotenv = require('dotenv')

dotenv.config()

const configs = Object.freeze({
  rabbit: {
    uri: process.env.RABBIT_URI || 'amqp://localhost:5672',
    queue_eat: process.env.QUEUE_EAT || 'eat',
    queue_wash: process.env.QUEUE_WASH || 'wash',
    exchange: process.env.EXCHANGE || 'exchange'
  },
  redis_uri: process.env.REDIS_URI || 'redis://localhost:6379',
})

module.exports = configs
